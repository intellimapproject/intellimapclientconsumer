module.controller('singInCtrl', function($scope, $ionicSideMenuDelegate, $state , config , $http, $location) {
    $scope.$on('$ionicView.enter', function () {
    $ionicSideMenuDelegate.canDragContent(false);
});
  console.log("Sing-In Controller");
  const url = config.domain + config.user + config.userRegistration;
  $scope.singInData = {};
  $scope.doSingIn = function(){
    console.log($scope.singInData);
    $http.post(url + "?name="+ $scope.singInData.name + "&email=" + $scope.singInData.email+ "&username=" + $scope.singInData.username + "&password=" + $scope.singInData.password)
        .then(function (response) {
          console.log(response);
          $location.path('/app/login');
        }, function (error) {
          popup.showPopup("Something went wrong. Try again later", "Oops!");
          console.error(error)
        });
  }
});
