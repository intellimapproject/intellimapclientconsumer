module.controller('AdvanceSearchCtrl', function ($scope, $stateParams, config, $http, localStorageService, searchService, $location, $timeout) {
  console.log("AdvanceSearchCtrl");

  $scope.search = {};
  $scope.tags = {};
  $scope.event = {};
  $scope.physical = {};

  $scope.openElement = function (closeElement, openElement, infoElement) {
    document.getElementById(closeElement).style.display = "none";
    document.getElementById(openElement).style.display = "block";
    document.getElementById(infoElement).style.display = "block";
  };

  $scope.closeElement = function (closeElement, openElement, infoElement) {
    document.getElementById(closeElement).style.display = "block";
    document.getElementById(openElement).style.display = "none";
    document.getElementById(infoElement).style.display = "none";
  };

  $scope.eventChecked = function () {
    document.getElementById("showEvent").style.display = "block";
    document.getElementById("showDateTimePicker").style.display = "none";
  };

  $scope.freeChecked = function () {
    document.getElementById("showEvent").style.display = "none";
    document.getElementById("showDateTimePicker").style.display = "block";
  };


  let today = moment().format('YYYY-MM-DD');
  document.getElementById("chooseDate").value = today;
  document.getElementById("chooseDateForEvent").value = today;

  let siteData = localStorageService.get('siteData');
  let tags = siteData.siteTags;

  let tagTypesNames = [];
  tags.forEach(tag => {
    if (tagTypesNames.indexOf(tag.tagType) === -1) {
      tagTypesNames.push(tag.tagType);
    }
  });

  $scope.equipments = [];
  tags.forEach(tag => {
    if (tag.tagType === "equipment") {
      $scope.equipments.push(tag);
    }
  });

  let tagTypes = [];
  tagTypesNames.forEach(tagType => {
    let tagTypeObject = {
      tagType: tagType,
      closeTagSearchElement: "close" + tagType + "Search",
      openTagSearchElement: "open" + tagType + "Search",
      tagInfoSearchElement: tagType + "InfoSearch",
      tags: []
    };
    tags.forEach(tag => {
      if (tag.tagType === tagTypeObject.tagType) {
        tagTypeObject.tags.push(tag)
      }
    });
    tagTypes.push(tagTypeObject);
  });

  $scope.tagTypes = tagTypes;

  /* prepare and send search request to server side*/
  $scope.startSearch = function () {
    let requestData = $scope.search;
    if ($scope.search.roomName !== undefined) {
      requestData = $scope.search;
    }
    else {
      requestData = {
        siteId: window.localStorage.getItem('siteId')
      };

      if ($scope.tags !== undefined) {
        requestData.tags = [];
        let tagTypesArray = [];
        let tagsTypes = $scope.tags;
        Object.keys(tagsTypes).map(function (key) {
          tagTypesArray.push({
            type: key,
            tags: tagsTypes[key]
          });
        });
        tagTypesArray.forEach(tagType => {
          let tagList = [];
          Object.keys(tagType.tags).map(function (key) {
            if (tagType.tags[key] === true) {
              tagList.push(key);
            }
          });
          requestData.tags.push({
              type: tagType.type,
              tags: tagList
            }
          )
        });
      }

      if ($scope.event.eventName !== undefined) {
        let startTimeSearch = document.getElementById("chooseDateForEvent").value + "T00:00:00.000Z";
        let endTimeSearch = document.getElementById("chooseDateForEvent").value + "T23:59:00.000Z";

        requestData.events = {
          name: $scope.event.eventName,
          startTime: startTimeSearch,
          endTime: endTimeSearch
        }
      }

      if (document.getElementById("showDateTimePicker").style.display === "block") {
        let startTime = document.getElementById("startTime").value;
        let endsTime = document.getElementById("endsTime").value;
        let StartTimeSearch = document.getElementById("chooseDate").value + "T" + startTime + ":00";
        let endTimeSearch = document.getElementById("chooseDate").value + "T" + endsTime + ":00";

        requestData.freeRooms = {
          startTime: StartTimeSearch,
          endTime: endTimeSearch
        }
      }

      if ($scope.physical !== undefined) {
        requestData.physical = {
          width: $scope.physical.width,
          height: $scope.physical.height,
          capacity: $scope.physical.capacity
        }
      }

    }

    $http.post(config.domain + config.search, requestData).success(
      function (responseData) {
        searchService.setResult(responseData.data);
        $location.path('/app/searchResults');
      })
  };

  $timeout(function () {
    tagTypes.forEach(tagType => {
      document.getElementById(tagType.openTagSearchElement).style.display = "none";
      document.getElementById(tagType.tagInfoSearchElement).style.display = "none";
    });
    document.getElementById("openGeneralSearch").style.display = "none";
    document.getElementById("generalInfoSearch").style.display = "none";
    document.getElementById("openTimeSearch").style.display = "none";
    document.getElementById("showEvent").style.display = "none";
    document.getElementById("timeInfoSearch").style.display = "none";
    document.getElementById("showDateTimePicker").style.display = "none";
    document.getElementById("openPhysical").style.display = "none";
    document.getElementById("choosePhysical").style.display = "none";
  }, 300);

});
