module.controller('SettingsCtrl', function ($scope, $stateParams, localStorageService) {

  if (window.localStorage.getItem('specialMode') === null) { //on first app run
    window.localStorage.setItem('specialMode', true);
  }
  if (window.localStorage.getItem('bleNavigation') === null) { //on first app run
    window.localStorage.setItem('bleNavigation', true);
  }

  let userSettings = localStorageService.get("user");
  console.log(userSettings);
  $scope.userData = userSettings;

  $scope.settingsList = [
    {text: "Special mode", checked: window.localStorage.getItem('specialMode') === 'true', icon: "icon ion-map"},
    {text: "Bluetooth navigation", checked: window.localStorage.getItem('bleNavigation') === 'true'}
  ];

  $scope.$watch('settingsList[0].checked', function (newValue, oldValue) {
    window.localStorage.setItem('specialMode', newValue);
  });

  $scope.$watch('settingsList[1].checked', function (newValue, oldValue) {
    window.localStorage.setItem('bleNavigation', newValue);
  });

});
