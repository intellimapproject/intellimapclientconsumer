module.controller('ChangeMapCtrl', function ($scope, $rootScope, $timeout, $state, $cordovaBeacon, $ionicHistory, $http, $location, config, localStorageService, popup, bleFactory) {
  console.log('ChangeMapCtrl');
  let bleScanFlag = true;
  let sensors = [];
  $scope.$on('$ionicView.afterEnter', function () {
    console.log("----afterEnter-----");
    bleSearch();
  });

  $scope.$on('$ionicView.beforeLeave', function () {
    console.log("----beforeLeave-----");
    stopBleSearch()
  });

  $scope.setSite = function (siteId) {
    $http.get(config.domain + config.siteObjects + siteId + config.getSiteInformationForClient)
      .then(function (response) {
        if (response.data.result === "OK") {
          stopBleSearch();
          $timeout(function () {
            localStorageService.remove('siteData');
            window.localStorage.removeItem('siteId');
            localStorageService.set('siteData', response.data.data);
            window.localStorage.setItem('siteId', siteId);
            window.location.reload();
            $location.path('/app/map');
          }, 200);
        }
        else {
          popup.showPopup("Something went wrong. Try again later", "Oops!");
          console.error(response);
        }
      });
  };

  $http.get(config.domain + config.getAllSitesData)
    .then(function (response) {
      console.log(response);
      if (response.data.result === "OK") {
        $scope.sites = response.data.data.sites;
        sensors = response.data.data.sensors;
        bleSearch();
      }
      else {
        popup.showPopup("Something went wrong. Try again later", "Oops!");
        console.error(response);
      }
    });

  function bleSearch() {
    if (bleFactory.isBleSearchOn()) {
      bleScanFlag = true;
      if (typeof cordova !== "undefined") {
        $http.get(config.domain + config.getAllSitesSensors).then(function (response) {
          if (response.data.result === "OK") {
            let sensors = response.data.data;
            if (sensors.length > 0) {
              bleFactory.initialBluetoothSearch();
              $rootScope.$on("$cordovaBeacon:didRangeBeaconsInRegion", function (event, pluginResult) {
                if (bleScanFlag) {
                  console.log("didRangeBeaconsInRegion on changeMapController");
                  console.log("didRangeBeaconsInRegion");
                  console.log(pluginResult.beacons.length);
                  pluginResult.beacons.forEach(function (beacon) {
                    let ble = bleFactory.findSensorOnList(beacon, sensors);
                    if (ble !== null) {
                      console.log("Beacon Detected");
                      console.log(ble);
                      stopBleSearch();
                      bleFactory.setLastDetectedSensor(ble);
                      popup.showConfirm("Do you want us to show your location?", "Beacon Detected", $scope.setSite, ble.siteUnitId);
                    }
                  });
                }
              });
              let bluetoothRegion = $cordovaBeacon.createBeaconRegion(sensors[0].identifier, sensors[0].uuid);
              bleFactory.startMonitoringBluetooth(bluetoothRegion);
              console.log("Start bluetooth search");
            }
            else {
              console.info("BLE not work. No sensors on DB");
            }

          }
          else {
            console.error("BLE not work. getAllSitesSensors fail");
          }
        });
      }
    } else {
      console.info("BLE search disable.");
    }
  }

  function stopBleSearch() {
    bleScanFlag = false;
  }

});
