module.controller('MapCtrl', function ($scope, $rootScope, $ionicPlatform, $cordovaBeacon, $http, $ionicScrollDelegate, $ionicFilterBar, $timeout, $location, $interval, popup, bleFactory, config, spaceUnitTypes, localStorageService, searchService, updateSiteData) {
  $ionicPlatform.ready(function () {
    console.log("MapCtrl");
    document.getElementById("mapClickedMenu").style.display = "none";
    document.getElementById("inputRoom2Go").style.display = "none";
    document.getElementById("clearRoom2").style.display = "none";
    updateSiteData.getSiteData();
    $scope.searchRoom = {
      name: ""
    };
    $scope.chooseRoom = {
      value: 0
    };

    let siteData, buildingEntrance, currentBuildingId, floorId, siteId, roomsForSearch, sensors, floorListOfThisSite;

    updateData();

    let floorsListOfThisBuilding = floorListOfThisSite.filter(object => {
      return object.buildingId === currentBuildingId;
    });

    let imgFootprintUp = new Image();
    imgFootprintUp.src = "img/footPrintUp.png";
    let imgFootprintDown = new Image();
    imgFootprintDown.src = "img/footPrintDown.png";
    let imgFootprintLeft = new Image();
    imgFootprintLeft.src = "img/footPrintLeft.png";
    let imgFootprintRight = new Image();
    imgFootprintRight.src = "img/footPrintRight.png";
    let imgFinish = new Image();
    imgFinish.src = "img/finish.png";
    const pathArcX = 120;
    const pathArcY = 40;
    const pathArcR = 18;

    let flourWidth;
    let flourHeight;
    let dataMap;
    let currentUserPosition = null;
    let clickedUserPosition = null;
    let lastPath = [];
    let eventToNavigate = null;
    let lastOnClickEvent;
    let userUnitPosition;
    let needToShowPopUp = true;
    let zoom = 100;
    let currentFloorNumber;
    let bluetoothRegion;
    let foorUpDownClicked = false;
    let previousBle = null;
    let bleScanFlag = true;
    //one unit size
    const unitWidth = 50;
    const unitHeight = 50;

    //css variables
    const borderStyle = "1px solid #C3C1AE";
    // const borderStyle = "2px solid #D8D3C3";
    //define the section thats hold the map

    let floorObject;
    let floorObjectName;

    let targetNavigationRoom = null;
    let detectingFirstBeacon = true;
    let lastDetectedSensor = bleFactory.getLastDetectedSensor();

    let searchResultRoom = null;
    let previousSearchResult = null;
    let unsubscribeFromCordovaBeacon = null;
    let checkEventsTime = config.checkEventsTime;
    if (lastDetectedSensor !== null) {
      if (siteId === lastDetectedSensor.siteId) {
        currentUserPosition = {
          x: lastDetectedSensor.coordX,
          y: lastDetectedSensor.coordY,
          floorId: Number(getFloorIdFromSpaceUnitId(lastDetectedSensor.spaceUnitId)),
          spaceUnitId: lastDetectedSensor.spaceUnitId
        }
      }
    }
    if (currentUserPosition === null) {
      currentUserPosition = {
        x: buildingEntrance.zones[0].topLeftX,
        y: buildingEntrance.zones[0].topLeftY,
        floorId: buildingEntrance.floorId,
        spaceUnitId: buildingEntrance.spaceUnitId
      };
    }

    const mapSection = document.getElementById("mapSection");

    function updateData() {
      siteData = localStorageService.get('siteData');
      buildingEntrance = siteData.buildingEntrance[0];
      currentBuildingId = buildingEntrance.buildingId;
      floorId = buildingEntrance.floorId;
      siteId = buildingEntrance.siteId;
      roomsForSearch = siteData.siteRoomsForSearch;
      sensors = siteData.siteSensors;
      floorListOfThisSite = siteData.floors;
    }

    /* get map data from server side */
    function getMap() {
      $http.get(config.domain + config.floorsObjects + floorId)
        .then(function (response) {
          if (response.data.result === "OK") {
            drawMap(response);
            searchResultRoom = searchService.getPaintRoom();
            if (searchResultRoom !== undefined) {
              callPaintRequiredRoom(searchResultRoom);
            }
            foorUpDownClicked = false;
          }
          else {
            requestToServerFailed(getMap.name, response);
          }
        }, function (error) {
          requestToServerFailed(getMap.name, error);
        });
    }

    /* loading next floor on path to show it to user */
    function loadNextFloor(pathCells, i, nextFloorId) {
      floorId = nextFloorId;
      $http.get(config.domain + config.floorsObjects + nextFloorId)
        .then(function (response) {
          if (response.data.result === "OK") {
            drawMap(response);
            drawPatch(pathCells, i + 2);
            popup.showPopup("You need to go to floor number " + currentFloorNumber, "");
          }
          else {
            requestToServerFailed(loadNextFloor.name, response);
          }
        }, function (error) {
          requestToServerFailed(loadNextFloor.name, error);
        });
    }


    /* rendering map according to server data */
    function drawMap(data) {
      while (mapSection.firstChild) {
        mapSection.removeChild(mapSection.firstChild);
      }
      let mapData = data.data.data;
      floorObject = mapData.floorObject;
      floorObjectName = floorObject.name;
      $scope.chooseRoom.value = floorObjectName;
      flourWidth = floorObject.zones[0].widthUnit;
      flourHeight = floorObject.zones[0].heightUnit;
      dataMap = mapData.map;
      currentFloorNumber = floorObject.name;
      drawFloor();
      drawRooms(dataMap);
      drawBorders(dataMap);
      updateLocation();
      if (targetNavigationRoom !== null) {
        getPath(currentUserPosition.spaceUnitId, targetNavigationRoom);
      }
    }


    /* rendering floor according to server data */
    function drawFloor() {
      mapSection.style.width = flourWidth * unitWidth + "px";
      mapSection.style.height = flourHeight * unitHeight + "px";
      for (let i = 0; i < flourWidth; i++) {
        for (let j = 0; j < flourHeight; j++) {
          let unit = document.createElement("canvas");
          unit.className = "mapUnit";
          unit.id = "x" + j + "y" + i;
          mapSection.appendChild(unit);
          document.getElementById("x" + j + "y" + i).setAttribute("type", "corridor");
        }
      }
    }

    /* rendering rooms according to server data */
    function drawRooms(roomsData) {
      roomsData.forEach(function (room) {
        room.zones.forEach(function (zone) {
          for (let i = zone.topLeftY; i < zone.topLeftY + zone.heightUnit; i++) {
            for (let j = zone.topLeftX; j < zone.topLeftX + zone.widthUnit; j++) {
              if (i === zone.topLeftY && j === zone.topLeftX + 1 && room.accessibility === 0) {
                let secondtCell = document.getElementById("x" + j + "y" + i);
                secondtCell.className = secondtCell.className + " accessibilityMapSymbol";
              }
              let cell = document.getElementById("x" + j + "y" + i);
              cell.className = cell.className + " " + room.type;
              cell.setAttribute("name", room.spaceUnitId);
              cell.setAttribute("type", room.type);
              cell.addEventListener('click', function () {
                clickedUserPosition = {
                  x: j,
                  y: i,
                  floorId: Number(getFloorIdFromSpaceUnitId(room.spaceUnitId)),
                  spaceUnitId: room.spaceUnitId
                }
              })
            }
          }
          drawRoomName(room, zone);
        })
      });
    }

    /* rendering rooms name according to server data */
    function drawRoomName(room, zone) {
      if (room.type !== spaceUnitTypes.corridor && room.type !== spaceUnitTypes.elevator
        && room.type !== spaceUnitTypes.stairs && room.type !== spaceUnitTypes.buildingEntrance) {
        let firstCell = document.getElementById("x" + zone.topLeftX + "y" + zone.topLeftY);
        let ctx = firstCell.getContext("2d");
        ctx.font = "70px Verdana";
        ctx.fillText(room.name, 100, 100);
      }
    }

    /* rendering rooms borders and entrance according to server data */
    function drawBorders(roomsData) {
      roomsData.forEach(function (room) {
        room.zones.forEach(function (zone) {
          for (let i = zone.topLeftY; i < zone.topLeftY + zone.heightUnit; i++) {
            for (let j = zone.topLeftX; j < zone.topLeftX + zone.widthUnit; j++) {
              let unit = document.getElementById("x" + j + "y" + i);
              if (unit.getAttribute("type") !== spaceUnitTypes.corridor) {
                //up
                drawBorder((i - 1), j, room, unit, "borderTop");
                //left
                drawBorder(i, (j - 1), room, unit, "borderLeft");
                //down
                drawBorder((i + 1), j, room, unit, "borderBottom");
                //right
                drawBorder(i, (j + 1), room, unit, "borderRight");
              }
            }
          }
        });

        //drawing the room entrance
        if (room.entrance !== undefined) {
          room.entrance.forEach(function (entrance) {
            let unit = document.getElementById("x" + entrance.xExitPosition + "y" + entrance.yExitPosition);
            switch (entrance.direction) {
              case "left":
                unit.style.borderLeft = "none";
                unit.setAttribute("roomEntranceDirection", "left"); //for connect the room to corridor at "generateCorridor" function
                break;
              case "right":
                unit.style.borderRight = "none";
                unit.setAttribute("roomEntranceDirection", "right"); //for connect the room to corridor at "generateCorridor" function
                break;
              case "up":
                unit.style.borderTop = "none";
                unit.setAttribute("roomEntranceDirection", "up"); //for connect the room to corridor at "generateCorridor" function
                break;
              case "down":
                unit.style.borderBottom = "none";
                unit.setAttribute("roomEntranceDirection", "down"); //for connect the room to corridor at "generateCorridor" function
                break;
              default:
            }
          })
        }
      })
    }

    /* rendering rooms borders according to server data */
    function drawBorder(i, j, room, unit, borderDirection) {
      if (!(room.type === spaceUnitTypes.elevator || room.type === spaceUnitTypes.stairs || room.type === spaceUnitTypes.buildingEntrance)) {
        let neighborCell = document.getElementById("x" + j + "y" + i);
        if (null !== neighborCell) {
          if (neighborCell.getAttribute("name") !== room.spaceUnitId) {
            unit.style[borderDirection] = borderStyle;
          }
        }
        else {
          unit.style[borderDirection] = borderStyle;
        }
      }
    }



    /* get path from serer side */
    function getPath(from, to) {
      if (from === to) {
        popup.showPopup("Your start location and target is same", "Oops!");
      } else {
        needToShowPopUp = false;
        $http.get(config.domain + config.getPath + from + "/" + to + "/" + getUserAccessibility())
          .then(function (response) {
            if (response.data.result === "OK") {
              let responseData = response.data.data;
              if (responseData === null) {
                popup.showPopup("No path found", "Oops!");
              }
              else {
                drawPatch(responseData, 0);
              }
            }
            else {
              requestToServerFailed(getPath.name, response);
            }
          }, function (error) {
            requestToServerFailed(getPath.name, error);
          });
      }
    }

    /* drawPatch according to server data */
    function drawPatch(pathCells, i) {
      let length = pathCells.length;
      let previousCellCoordinate = null;
      let previousCell = null;
      for (; i < length; i++) {
        if (Number(getFloorIdFromSpaceUnitId(pathCells[i])) === Number(floorId)) {
          let spaceUnitsCells = document.getElementsByName(pathCells[i]);
          spaceUnitsCells.forEach(function (cell) {
            let cellType = cell.getAttribute("type");
            if (cellType === spaceUnitTypes.corridor) {
              let cellCoordinate = getCanvasCoordinates(cell);
              let cellSpaceUnitId = cell.getAttribute('name');
              if (cellSpaceUnitId !== currentUserPosition.spaceUnitId) {
                if (previousCellCoordinate !== null) {
                  drawPathDirection(previousCellCoordinate, cellCoordinate, previousCell);
                }
                if (i + 2 === pathCells.length) {
                  drawFinish(cell)
                }
                previousCell = cell;
                previousCellCoordinate = cellCoordinate;
              }
            }
            else if (cellType === spaceUnitTypes.elevator || cellType === spaceUnitTypes.stairs) {
              let cellCoordinate = getCanvasCoordinates(cell);
              drawPathDirection(previousCellCoordinate, cellCoordinate, previousCell);
              if (pathCells[i + 1] !== undefined) {
                let nextFloorId = getFloorIdFromSpaceUnitId(pathCells[i + 1]);
                let temp = i;
                cell.onclick = function () {
                  loadNextFloor(pathCells, temp, nextFloorId);
                }
              }
            }
          });
        }
      }
    }

    function drawPathDirection(previousCellCoordinate, cellCoordinate, previousCell) {
      if (previousCellCoordinate !== null) {
        if (Number(previousCellCoordinate.x) > Number(cellCoordinate.x)) {
          drawLeftArrow(previousCell);
        }
        else if (Number(cellCoordinate.x) > Number(previousCellCoordinate.x)) {
          drawRightArrow(previousCell);
        }
        else if (Number(previousCellCoordinate.y) < Number(cellCoordinate.y)) {
          drawDownArrow(previousCell);
        }
        else {
          drawUpArrow(previousCell);
        }
      }
    }

    //canvas methods
    function drawPathOnCell(cell, color) {
      let ctx = cell.getContext("2d");
      ctx.beginPath();
      ctx.fillStyle = color;
      ctx.arc(pathArcX, pathArcY, pathArcR, 0, 2 * Math.PI);
      ctx.fill();
      ctx.stroke();
      lastPath.push(cell);
    }

    function drawFinish(cell) {
      let ctx = cell.getContext("2d");
      ctx.drawImage(imgFinish, 60, 20);
      lastPath.push(cell);
    }

    function drawRightArrow(cell) {
      let ctx = cell.getContext("2d");
      ctx.drawImage(imgFootprintRight, 60, 20);
      lastPath.push(cell);
    }

    function drawLeftArrow(cell) {
      let ctx = cell.getContext("2d");
      ctx.drawImage(imgFootprintLeft, 60, 20);
      lastPath.push(cell);
    }

    function drawUpArrow(cell) {
      let ctx = cell.getContext("2d");
      ctx.drawImage(imgFootprintUp, 60, 20, 140, 80);
      lastPath.push(cell);
    }

    function drawDownArrow(cell) {
      let ctx = cell.getContext("2d");
      ctx.drawImage(imgFootprintDown, 60, 20, 140, 80);
      lastPath.push(cell);
    }

    function getCanvasCoordinates(corridorCell) {
      let str = corridorCell.id.substr(1);
      let result = str.split('y');
      return {
        x: result[0],
        y: result[1]
      }
    }

    function callPaintRequiredRoom(data) {
      if (foorUpDownClicked === false) {
        if (previousSearchResult === null) {
          previousSearchResult = data;
        }
        else {
          paintRequiredRoom(previousSearchResult, true);
        }
        searchResultRoom = data;
        paintRequiredRoom(data, false);
      }

      else if (foorUpDownClicked === true && data.floorId === floorId) {
        paintRequiredRoom(data, false);
      }
    }

    $rootScope.$on('getFloorNumberForDrawing', function (event, data) {
      getFloorByNumber(data.floorName);
    });

    $rootScope.$on('navigateTo', function (event, data) {
      $timeout(function () {
        clearCurrentPath();
        targetNavigationRoom = data;
        getPath(currentUserPosition.spaceUnitId, data);
      }, 300);
    });

    $rootScope.$on('siteDataUpdate', function (event, data) {
      updateData();
    });


    function paintRequiredRoom(requiredRoom, flagClear) {
      if (requiredRoom !== undefined) {
        if (flagClear === false) {
          let zones = requiredRoom.zones[0];
          for (let i = zones.topLeftY; i < zones.topLeftY + zones.heightUnit; i++) {
            for (let j = zones.topLeftX; j < zones.topLeftX + zones.widthUnit; j++) {
              let cell = document.getElementById("x" + j + "y" + i);
              cell.style.backgroundColor = "#FFFF66";
            }
          }
        }
        else if (flagClear === true) {
          let zones = requiredRoom.zones[0];
          for (let i = zones.topLeftY; i < zones.topLeftY + zones.heightUnit; i++) {
            for (let j = zones.topLeftX; j < zones.topLeftX + zones.widthUnit; j++) {
              let cell = document.getElementById("x" + j + "y" + i);
              cell.style.backgroundColor = "#F7EEDA";
            }
          }
        }
      }
    }


    $scope.showCoords = function ($event) {
      lastOnClickEvent = $event;
      let x_coord = $event.clientX;
      let y_coord = $event.clientY;
      let spaceUnitMenu2 = document.getElementById('mapClickedMenu');

      if (y_coord > 580 && spaceUnitMenu2.style.display === 'none' || x_coord > 330) {

      }

      else if (spaceUnitMenu2.style.display === 'none') {
        document.getElementById('showFooterMenu').style.display = 'none';
        spaceUnitMenu2.style.display = 'block';
      }

      else if (y_coord > 580 && spaceUnitMenu2.style.display === 'block') {
        document.getElementById('showFooterMenu').style.display = 'none';
      }
      else {
        spaceUnitMenu2.style.display = 'none';
        document.getElementById('showFooterMenu').style.display = 'block';
      }

    };

    function clearCurrentPath() {
      if (searchResultRoom !== null) {
        paintRequiredRoom(searchResultRoom, true);
      }
      lastPath.forEach(function (cell) {
        let ctx = cell.getContext("2d");
        ctx.clearRect(0, 0, cell.width, cell.height);
      });
    }

    $scope.callClearPath = function () {
      targetNavigationRoom = null;
      clearCurrentPath();
    };

    $scope.navigateTo = function () {
      clearCurrentPath();
      targetNavigationRoom = null;
      $scope.returnToUserPosition();
      getPath(currentUserPosition.spaceUnitId, clickedUserPosition.spaceUnitId);
    };

    function search(searchRoom) {
      roomsForSearch.forEach(function (room) {
        if (room.name === searchRoom) {
          clearCurrentPath();
          getPath(currentUserPosition.spaceUnitId, room.spaceUnitId);
        }
      })
    }

    $scope.searchRoom2Go = function () {
      $scope.returnToUserPosition();
      roomsForSearch.forEach(function (room) {
        if (room.name === $scope.searchRoom.name) {
          clearCurrentPath();
          targetNavigationRoom = room.spaceUnitId;
          getPath(currentUserPosition.spaceUnitId, room.spaceUnitId);
        }
      })
    };

    $scope.cancelGoTo = function () {
      document.getElementById("goInput").style.display = "block";
      document.getElementById("inputRoom2Go").style.display = "none";
      document.getElementById("clearRoom").style.display = "block";
      document.getElementById("clearRoom2").style.display = "none";
    };

    $scope.updateLocation = function () {
      currentUserPosition = clickedUserPosition;
      updateLocation();
    };

    /* updating user location on map */
    function updateLocation() {
      if (userUnitPosition !== undefined) { //remove user position icon from previous position
        userUnitPosition.style.backgroundImage = 'none';
      }
      if (currentUserPosition.floorId === floorId) {
        let unit = document.getElementById("x" + currentUserPosition.x + "y" + currentUserPosition.y);
        unit.style.backgroundImage = "url('img/currentLocation.png')";
        unit.style.backgroundRepeat = "no-repeat";
        userUnitPosition = unit;
        $ionicScrollDelegate.scrollTo(currentUserPosition.x * 50, currentUserPosition.y * 50, true);
      }
    }

    $scope.incrementMapZoom = function () {
      needToShowPopUp = false;
      if (zoom < 200) {
        zoom += 10;
        mapSection.style.zoom = zoom + "%";
      }
    };

    $scope.decrementMapZoom = function () {
      needToShowPopUp = false;
      if (zoom > 60) {
        zoom -= 10;
        mapSection.style.zoom = zoom + "%";
      }
    };

    $scope.floorUp = function () {
      foorUpDownClicked = true;

      let value = $scope.chooseRoom.value;
      value++;
      if (floorsListOfThisBuilding !== null) {
        let floor = floorsListOfThisBuilding.filter(object => {
          return Number(object.name) === value;
        });
        if (floor.length === 1) {
          $scope.chooseRoom.value++;
          getFloorByNumber($scope.chooseRoom.value);
        }
      }
      else { // floor up validation error
        console.error("Floor list is null");
        $scope.chooseRoom.value++;
        getFloorByNumber($scope.chooseRoom.value);
      }
    };

    $scope.floorDown = function () {
      foorUpDownClicked = true;

      needToShowPopUp = false;
      let value = $scope.chooseRoom.value;
      value--;
      if (floorsListOfThisBuilding !== null) {
        let floor = floorsListOfThisBuilding.filter(object => {
          return Number(object.name) === value;
        });
        if (floor.length === 1) {
          $scope.chooseRoom.value--;
          getFloorByNumber($scope.chooseRoom.value);
        }
      }
      else { // floor up validation error
        console.error("Floor list is null");
        $scope.chooseRoom.value--;
        getFloorByNumber($scope.chooseRoom.value);
      }
    };

    function getFloorByNumber(floorNumber) {
      floorNumber = floorNumber.toString();
      if (floorsListOfThisBuilding !== undefined) {
        floorsListOfThisBuilding.forEach(function (floor) {
          if (floor.name === floorNumber) {
            floorId = floor.floorId;
            getMap();
          }
        })
      }
      else {
        console.error("floorsListOfThisBuilding is undefined");
        popup.showPopup("Something went wrong. Try again later", "Oops!");
      }
  }

    function getUserAccessibility() {
      let accessibility;
      let specialMode = window.localStorage.getItem('specialMode');
      if (specialMode === null) {
        accessibility = 0;
      }
      else {
        if (window.localStorage.getItem('specialMode') === "true") {
          accessibility = 0;
        }
        else {
          accessibility = 1;
        }
      }
      return accessibility;
    }

    $scope.showFilterBar = function () {
      let filterBarInstance = $ionicFilterBar.show({
        update: function (filteredItems, filterText) {
          if (filterText === undefined) {
            console.log("Nothing to show");
          }
          else {
            search(filterText);
          }
        }
      });
    };

    function requestToServerFailed(functionName, error) {
      console.error(functionName + " failed");
      console.error(error);
      popup.showPopup("Something went wrong. Try again later", "Oops!");
    }

    $scope.showGoInput = function () {
      needToShowPopUp = false;
      document.getElementById("inputRoom2Go").style.display = "block";
      document.getElementById("goInput").style.display = "none";
      document.getElementById("clearRoom").style.display = "none";
      document.getElementById("clearRoom2").style.display = "block";
    };

    $scope.returnToUserPosition = function () {
      floorId = currentUserPosition.floorId;
      getMap();
    };

    function getFloorIdFromSpaceUnitId(spaceUnitId) {
      return spaceUnitId.split('.')[2];
    }


    function updateUserLocationFromBle(ble) {
      if (previousBle === null || ble.major !== previousBle.major) {
        previousBle = ble;
        popup.toast("Your position updated according to bluetooth");
        let spaceUnitId = ble.spaceUnitId;
        let floorSpaceUnitId = Number(getFloorIdFromSpaceUnitId(spaceUnitId));
        currentUserPosition = {
          x: ble.coordX,
          y: ble.coordY,
          floorId: Number(getFloorIdFromSpaceUnitId(ble.spaceUnitId)),
          spaceUnitId: ble.spaceUnitId
        };
        updateLocation();
        if (targetNavigationRoom !== null) {
          if (floorSpaceUnitId !== floorId) {
            floorId = floorSpaceUnitId;
            getMap();
          }
          else {
            clearCurrentPath();
            getPath(currentUserPosition.spaceUnitId, targetNavigationRoom);
          }
        }
        else if (detectingFirstBeacon) {
          if (floorObject !== undefined) {
            detectingFirstBeacon = false;
            changeMapToView(floorSpaceUnitId)
          }
        }
      }
    }

    function changeMapToView(floorSpaceUnitId) {
      if (floorSpaceUnitId !== floorObject.floorId) {
        floorId = floorSpaceUnitId;
        popup.showConfirm("We detected that you are on another floor. Do you won't we show your location?", "BLE", getMap);
      }
    }

    /* initial ble search */
    function bleSearch() {
      if (bleFactory.isBleSearchOn()) {
        console.info("ble search start on mapController");
        if (typeof cordova !== "undefined") {
          if (sensors.length > 0) {
            bleFactory.initialBluetoothSearch();
            unsubscribeFromCordovaBeacon = $rootScope.$on("$cordovaBeacon:didRangeBeaconsInRegion", function (event, pluginResult) {
              if (bleScanFlag && bleFactory.isBleSearchOn()) {
                //don't remove this console.info line. It's important to debug if bluetooth not work
                // console.info("didRangeBeaconsInRegion on mapController");
                // console.info("bleScanFlag is " + bleScanFlag);
                // console.info(pluginResult.beacons.length);
                pluginResult.beacons.forEach(function (beacon) {
                  let ble = bleFactory.findSensorOnList(beacon, sensors);
                  if (ble !== null) {
                    //don't remove this console.info line. It's important to debug if bluetooth not work
                    // console.info("Beacon Detected");
                    // console.info(ble);
                    updateUserLocationFromBle(ble);
                  }
                });
              }
            });
            bluetoothRegion = $cordovaBeacon.createBeaconRegion(sensors[0].identifier, sensors[0].uuid);
            bleFactory.startMonitoringBluetooth(bluetoothRegion);
            console.info("Start bluetooth search");
          }
          else {
            console.info("BLE not work. No sensors on site");
          }
        }
        else {
          console.info("Cordova not supported. Ble not work.");
        }
      } else {
        console.info("BLE search disable.");
      }
    }

    function stopBleSearch() {
      bleScanFlag = false;
      bleFactory.stopMonitoringBluetooth(bluetoothRegion);
      if (unsubscribeFromCordovaBeacon !== null) {
        unsubscribeFromCordovaBeacon();
      }
    }


    function startCheckingEventInNearTime(checkEventsTime) {
      checkEventInNearTime();
      $interval(checkEventInNearTime, checkEventsTime * 60000);
    }

    /*check and notify user if he/she have event in near time */
    function checkEventInNearTime() {
      let startTime = "T00:00:00";
      let endTime = "T23:59:00";
      let checkEventFlag = true;
      let now = new Date();
      let day = ("0" + now.getDate()).slice(-2);
      let month = ("0" + (now.getMonth() + 1)).slice(-2);
      let dateValue = now.getFullYear() + "-" + (month) + "-" + (day);
      let startSchedule = dateValue + startTime;
      let endSchedule = dateValue + endTime;
      let siteId = window.localStorage.getItem('siteId');
      if (siteId === null) {
        popup.showPopup("Please choose site to see your schedule", "Warning");
      }
      $http.get(config.domain + config.userSchedule + '/' + siteId + '/' + startSchedule + '/' + endSchedule).then(function (response) {
        if (response.data.result === "OK") {
          let events = response.data.data;
          let now = new Date();
          let maxTime = moment(now).add(checkEventsTime, 'm').toDate();
          events.forEach(function (event) {
            let eventStartTime = new Date(event.dateTimeStart.slice(0, 19)); //todo find a better solution
            if (now < eventStartTime && maxTime > eventStartTime) {
              if (checkEventFlag) {
                eventToNavigate = event;
                popup.showConfirm("We detected that you have any event. Do you wont to navigate here? ", event.name, navigateToEvent);
                checkEventFlag = false;
              }
            }
          })
        }
      }, function (response) {
        if (response.data.error === "Unauthorized") {
          console.info("Unauthorized. Trying to login...");
          let username = window.localStorage.getItem('username');
          let password = window.localStorage.getItem('password');
          const url = config.domain + config.user + config.login;
          $http.post(url + "?&username=" + username + "&password=" + password)
            .then(function (response) {
              console.info("login done");
              console.info(response);
              checkEventInNearTime();
            }, function (error) {
              if (error.status === 401) {
                popup.showPopup("Please login", "Unauthorized");
                $location.path('/app/login');
              }
              else {
                popup.showPopup("Something went wrong. Try again later", "Oops!");
                console.error(error);
                $location.path('/app/login');
              }
            });
        }
      });
    }

    function navigateToEvent() {
      $rootScope.$broadcast('navigateTo', eventToNavigate.spaceUnitIds[0]);
    }

    $scope.$on('$ionicView.afterEnter', function () {
      bleScanFlag = true;
      bleSearch();
    });

    $scope.$on('$ionicView.beforeLeave', function () {
      stopBleSearch()
    });

    getMap();
    bleSearch();
    startCheckingEventInNearTime(checkEventsTime);

  });
});
