module.controller('searchResultsCtrl', function ($scope, $stateParams, $http, searchService, config, $location, $rootScope, localStorageService) {
  console.log("searchResultsCtrl");
  let results = searchService.getResult();
  const siteId = window.localStorage.getItem('siteId');
  let siteData = localStorageService.get('siteData');
  let buildings = siteData.buildings;
  let floors = siteData.floors;
  results.forEach(result => {
    buildings.forEach(building => {
      if (result.buildingId === building.buildingId) {
        result.buildingName = building.name;
      }
    });
    floors.forEach(floor => {
      if (result.floorId === floor.floorId) {
        result.floorName = floor.name;
      }
    });
  });
  $scope.results = results;
  $scope.counter = results.length;

  $scope.showResultOnMap = function (unitData) {
    searchService.setPaintRoom(unitData);
    $rootScope.$broadcast('getFloorNumberForDrawing', unitData);
    $location.path('/app/map');
  };

  $scope.navigateToResult = function (unitData) {
    $rootScope.$broadcast('navigateTo', unitData);
    $location.path('/app/map');
  };


});

