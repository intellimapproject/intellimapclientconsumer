// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'jett.ionic.filter.bar', 'ngCordova', 'LocalStorageModule', 'ionic-datepicker'])

  .run(function ($ionicPlatform) {
    $ionicPlatform.ready(function () {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);

      }
      if (window.StatusBar) {
        // org.apache.cordova.statusbar required
        StatusBar.styleDefault();
      }
    });
  })

  .constant('config', {
    domain: 'https://intelli-map-server.herokuapp.com',
    //domain:'http://localhost:3000',
    //  domain: 'https://intelli-map-server-test.herokuapp.com',
    siteObjects: '/api/v1/mapConsumer/site/',
    floorsObjects: '/api/v1/mapConsumer/floor/',
    getPath: '/api/v1/mapConsumer/getPath/',
    getAllSitesData: '/api/v1/mapConsumer/getAllSitesData',
    getSiteInformationForClient: '/siteInformationForClient',
    sensors: '/sensors',
    getAllSitesSensors: "/api/v1/mapConsumer/sensors/getAllSitesSensors",
    user: '/api/v1/user',
    userRegistration: '/register',
    login: '/login',
    search: '/api/v1/mapConsumer/search',
    userSchedule: '/api/v1/event/getEventsForLoggedInUser',
    checkEventsTime: 15  //in minutes
  })

  .constant('spaceUnitTypes', {
    site: "site",
    building: "building",
    floor: "floor",
    room: "room",
    corridor: "corridor",
    elevator: "elevator",
    stairs: "stairs",
    buildingEntrance: "buildingEntrance"
  })

  .config(function (localStorageServiceProvider) {
    localStorageServiceProvider.setPrefix('IntelliMapClientConsumer');
  })

  .factory('updateSiteData', function ($http, $rootScope, localStorageService, config) {
    let factory = {};
    factory.getSiteData = function () {
      $http.get(config.domain + config.siteObjects + window.localStorage.getItem('siteId')
        + config.getSiteInformationForClient).then(function (response) {
        localStorageService.set('siteData', response.data.data);
        $rootScope.$broadcast('siteDataUpdate');
      });
    };
    return factory;
  })

  .factory('popup', function ($ionicPopup) {
    let factory = {};
    factory.showPopup = function (popupText, popupTitle) {
      let alertPopup = $ionicPopup.alert({
        title: popupTitle,
        template: popupText
      });
      alertPopup.then(function (res) {
      });
    };

    factory.showConfirm = function (popupText, popupTitle, action, arg) {
      let confirmPopup = $ionicPopup.confirm({
        title: popupTitle,
        template: popupText
      });
      confirmPopup.then(function (res) {
        if (res) {
          action(arg);
        }
      });
    };

    factory.toast = function (message) {
      if (typeof cordova !== "undefined") {
        window.plugins.toast.showWithOptions(
          {
            message: message,
            duration: "long", // which is 2000 ms. "long" is 4000. Or specify the nr of ms yourself.
            position: "bottom",
            addPixelsY: -40  // added a negative value to move it up a bit (default 0)
          });
      }
      else {
        console.info("Cordova not supported. Toast not work.");
      }

    };
    return factory;
  })

  .factory('bleFactory', function ($cordovaBeacon) {
    let factory = {};
    let lastDetectedSensor = null;

    factory.isBleSearchOn = function () {
      let isOn;
      let specialMode = window.localStorage.getItem('bleNavigation');
      if (specialMode === null) {
        isOn = true;
      }
      else {
        isOn = window.localStorage.getItem('bleNavigation') === "true";
      }
      console.log("isBleSearchOn:" + isOn);
      return isOn;
    };


    factory.initialBluetoothSearch = function () {
      let permissions = cordova.plugins.permissions;
      let list = [
        permissions.BLUETOOTH_PRIVILEGED,
        permissions.BLUETOOTH_ADMIN,
        permissions.BLUETOOTH,
        permissions.ACCESS_COARSE_LOCATION
      ];
      permissions.checkPermission(permissions.ACCESS_COARSE_LOCATION, function (status) {
        console.log(status);
        if (!status.hasPermission) {
          permissions.requestPermissions(list, function (status) {
            if (!status.hasPermission) {
              console.log(status);
            }
            else {
            }
            enableBluetooth();
          }, function (error) {
            console.error(error);
            popup.showPopup(error, "Oops!");
          })
        }
        else {
          enableBluetooth();
        }
      }, function (error) {
        console.error(error);
        popup.showPopup(error, "Oops!");
      });
    };

    factory.startMonitoringBluetooth = function (bluetoothRegion) {
      $cordovaBeacon.isBluetoothEnabled()
        .then(function (isEnabled) {
          if (isEnabled) {
            console.info("Is ble enable:" + isEnabled);
            $cordovaBeacon.startRangingBeaconsInRegion(bluetoothRegion);
            console.info("Ble work");
          }
          else {
            console.error("Ble not work");
          }
        });
    };

    factory.stopMonitoringBluetooth = function (bluetoothRegion) {
      if (bluetoothRegion !== undefined) {
        $cordovaBeacon.stopRangingBeaconsInRegion(bluetoothRegion);
      }
    };

    factory.findSensorOnList = function (sensor, sensorList) {
      let ble = null;
      sensorList.forEach(function (object) {
        if (sensor.uuid === object.uuid && Number(sensor.major) === object.major
          && Number(sensor.minor) === object.minor) {
          ble = object;
        }
      });
      return ble;
    };

    factory.setLastDetectedSensor = function (sensor) {
      lastDetectedSensor = sensor;
    };

    factory.getLastDetectedSensor = function () {
      return lastDetectedSensor;
    };

    function enableBluetooth() {
      $cordovaBeacon.isBluetoothEnabled()
        .then(function (isEnabled) {
          if (!isEnabled) {
            $cordovaBeacon.enableBluetooth();
          }
        });
    }

    return factory;
  })

  .service('searchService', function () {
    let searchResult = {};
    let viewRoom;

    let setResult = function (result) {
      searchResult = result;
    };

    let getResult = function () {
      return searchResult;
    };

    let setPaintRoom = function (result) {
      viewRoom = result;
    };

    let getPaintRoom = function () {
      return viewRoom;
    };

    return {
      getResult: getResult,
      setResult: setResult,
      setPaintRoom: setPaintRoom,
      getPaintRoom: getPaintRoom
    };
  })

  .config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider

      .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'templates/menu.html',
        controller: 'AppCtrl'
      })

      .state('app.map', {
        url: '/map',
        views: {
          'menuContent': {
            templateUrl: 'templates/map.html',
            controller: 'MapCtrl'
          }
        }
      })

      .state('app.advanceSearch', {
        url: '/advanceSearch',
        views: {
          'menuContent': {
            templateUrl: 'templates/advanceSearch.html',
            controller: 'AdvanceSearchCtrl'
          }
        }
      })

      .state('app.schedule', {
        url: '/schedule',
        views: {
          'menuContent': {
            templateUrl: 'templates/schedule.html',
            controller: 'ScheduleCtrl'
          }
        }
      })

      .state('app.settings', {
        url: '/settings',
        views: {
          'menuContent': {
            templateUrl: 'templates/settings.html',
            controller: 'SettingsCtrl'
          }
        }
      })

      .state('app.logOut', {
        url: '/logOut',
        views: {
          'menuContent': {
            templateUrl: 'templates/logOut.html',
            controller: 'LogOutCtrl'
          }
        }
      })

      .state('app.login', {
        url: '/login',
        views: {
          'menuContent': {
            templateUrl: 'templates/login.html',
            controller: 'LoginCtrl'
          }
        }
      })

      .state('app.singIn', {
        url: '/singIn',
        views: {
          'menuContent': {
            templateUrl: 'templates/singIn.html',
            controller: 'singInCtrl'
          }
        }
      })

      .state('app.searchResults', {
        url: '/searchResults',
        views: {
          'menuContent': {
            templateUrl: 'templates/searchResults.html',
            controller: 'searchResultsCtrl'
          }
        }
      })

      .state('app.changeMap', {
        url: '/changeMap',
        views: {
          'menuContent': {
            templateUrl: 'templates/changeMap.html',
            controller: 'ChangeMapCtrl'
          }
        }
      });

    // if none of the above states are matched, use this as the fallback
    let userId = window.localStorage.getItem('userId');
    if (userId === null) {
      $urlRouterProvider.otherwise('/app/login');
    }
    else {
      let siteId = window.localStorage.getItem('siteId');
      if (siteId === null) {
        $urlRouterProvider.otherwise('/app/changeMap');
      }
      else {
        $urlRouterProvider.otherwise('/app/map');
      }
    }
  });
