module.controller('LoginCtrl', function($scope,$ionicSideMenuDelegate,$state ,config , $http, $location, popup, localStorageService) {
  $scope.$on('$ionicView.enter', function () {
    $ionicSideMenuDelegate.canDragContent(false);
});
  console.log("Login Controller");
  const url = config.domain + config.user + config.login;
  $scope.loginData = {};
  $scope.doLogin = function(){
    console.log($scope.loginData);
    $http.post(url + "?&username=" + $scope.loginData.username + "&password=" + $scope.loginData.password)
        .then(function (response) {
          console.log(response.data.data);
          localStorageService.set("user", response.data.data);
          window.localStorage.setItem('userId',response.data.data.id);
          window.localStorage.setItem('username',$scope.loginData.username);
          window.localStorage.setItem('password',$scope.loginData.password);
          $location.path('/app/changeMap');
        }, function (error) {
          if(error.status === 401){
            popup.showPopup("Wrong password or username", "Oops!");
          }
          else{
            popup.showPopup("Something went wrong. Try again later", "Oops!");
            console.error(error)
          }

        });
  }
});

