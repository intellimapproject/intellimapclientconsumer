module.controller('ScheduleCtrl', function ($scope, $stateParams, config, $http, popup, $rootScope, $location) {
  console.log("ScheduleCtrl");
  let today = moment().format('YYYY-MM-DD');
  document.getElementById("chooseDateForEvents").value = today;
  let startTime = "T00:00:00";
  let endTime = "T23:59:00";

  document.getElementById("chooseDateForEvents").addEventListener("input", inputDate);
  $scope.events = {};

  function inputDate() {
    getUserEvents();
  }

  $scope.$on('$ionicView.afterEnter', function () {
    console.log("$scope.$on");
    getUserEvents();
  });

  function getUserEvents() {
    let dateValue = document.getElementById("chooseDateForEvents").value;
    let startSchedule = dateValue + startTime;
    let endSchedule = dateValue + endTime;
    console.log("getUserEvents");
    console.log(document.getElementById("chooseDateForEvents").value);
    console.log("startDate");
    console.log(startSchedule);
    let siteId = window.localStorage.getItem('siteId');
    if (siteId === null) {
      popup.showPopup("Please choose site to see your schedule", "Warning");
    }
    $http.get(config.domain + config.userSchedule + '/' + siteId + '/' + startSchedule + '/' + endSchedule).then(function (response) {
      if (response.data.result === "OK") {
        console.log(response.data.data);
        $scope.events = response.data.data;
      }
    }, function (response) {
      if (response.data.error === "Unauthorized") {
        console.info("Unauthorized. Trying to login...");
        let username = window.localStorage.getItem('username');
        let password = window.localStorage.getItem('password');
        const url = config.domain + config.user + config.login;
        $http.post(url + "?&username=" + username + "&password=" + password)
          .then(function (response) {
            console.info("login done");
            console.info(response);
            getUserEvents();
          }, function (error) {
            if (error.status === 401) {
              popup.showPopup("Please login", "Unauthorized");
              $location.path('/app/login');
            }
            else {
              popup.showPopup("Something went wrong. Try again later", "Oops!");
              console.error(error);
              $location.path('/app/login');
            }
          });
      }
    });
  }

  $scope.navigateToEvent = function (unitData) {
    console.log(unitData);
    $rootScope.$broadcast('navigateTo', unitData);
    $location.path('/app/map');
  };
});
